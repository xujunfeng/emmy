#lang racket

;; Configuration for different SMT solvers
;; Currently supporting: Z3, CVC4

(provide (all-defined-out))

;; Read environment variable for solver name
(define solver (make-parameter
                (if (getenv "PROVER_SMT_NAME")
                    (string->symbol (string-upcase (getenv "PROVER_SMT_NAME")))
                    #f)))
(define solver-path (make-parameter
                     (if (getenv "PROVER_SMT_PATH")
                         (string->symbol (getenv "PROVER_SMT_PATH"))
                         #f)))
(define time-limit (make-parameter
                    (if (getenv "PROVER_TIME_LIMIT")
                        (exact-round
                         (string->number (getenv "PROVER_TIME_LIMIT")))
                        200)))

(define known-solvers '(Z3 CVC4))

;; Solver-specific command line arguments
(define (solver-args)
  (match (solver)
    ['Z3
     '("-in")]
    
    ['CVC4
     `("--lang=smt2"
       ,(~a "--tlimit=" (time-limit))

       ;; These arguments are taken from: http://lara.epfl.ch/~reynolds/VMCAI2015-ind/
       "--quant-ind"
       "--quant-cf"
       "--conjecture-gen"
       "--conjecture-gen-per-round=3"
       "--full-saturate-quant")]

    [_ '()]))

;; Find path to specific solver
;; If path is given use the path, otherwise search the executable in $PATH
(define (find-solver-path)
  (define path
    (cond
      [(solver-path) => values]
      [(solver) => (lambda (solver)
                     (match solver
                       ['Z3 (find-executable-path "z3")]
                       ['CVC4 (find-executable-path "cvc4")]
                       [_ #f]))]
      [#t #f]))
  (if path
      path
      (error "Error: Cannot find path to SMT solver.")))

;; SMT-LIB script commands to be executed for specific solvers
(define (solver-specific-script)
  (match (solver)
    ['Z3
     `((set-option :timeout ,(time-limit)))]
    
    ['CVC4 '()]

    [_ '()]))
