#lang racket

;; Define the module language in which proofs are written
;; We replace the default `#%module-begin' with our own,
;;   which reads a single `Proof' s-expression
;;   and checks the correctness of the proof

(provide (rename-out [module-begin #%module-begin]))

(require "./check-loop.rkt")

(define-syntax-rule (module-begin stmt ...)
  (#%module-begin
   (check-loop '() '() '() '(stmt ...))))
