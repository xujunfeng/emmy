# Emmy

Emmy is a proof assistant for reasoning about programs.

Emmy supports:

- Propositional and (many-sorted) first-order logic
- Recursive function definitions
- Data structures
- Induction over data structures and function definitions
- 'Stylised' proofs

Emmy uses an external SMT solver when checking the correctness of proofs.
We recommend using [Z3](https://rise4fun.com/z3/)([GitHub repository](https://github.com/Z3Prover/z3)).
However, we also support [CVC4](http://cvc4.cs.stanford.edu/web/).

## How to Install Emmy

1. Install [Racket](https://racket-lang.org/).

2. Install Z3 (recommended) or CVC4.
   Make sure that you can run Z3 by `z3` or CVC4 by `cvc4`.

3. Run the following commands:
```
git clone https://gitlab.com/xujunfeng/emmy.git
cd emmy/emmy
raco pkg install
```

4. Done

## Running Emmy

If you have installed Emmy via `raco pkg install`,
you can run Emmy by running:
```
racket -l emmy -- <Arguments>
```

### Command Line Arguments

- `--json`: use JSON as the IO format.
- `--s-expression`**(default)**: use s-expression as IO format.
- `-s <NAME>` `--solver <NAME>`: name of the SMT solver to use (either Z3 or CVC4).
- `-p <PATH>` `--solver-path <PATH>`: path to the executable of the SMT solver.
- `-t <INTEGER>` `--time-limit <INTEGER>`: the time limit per call to SMT solver.
- `--deep`: add steps obtained in nested subproofs to the set of checked steps
            (see definition 6.14 in the report).