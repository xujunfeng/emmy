#lang reader "example-reader.rkt"

{Declare
 [Data Code [lf Int] [nd]]
 [Data Tree [node Tree Tree] [leaf Int]]
 [Data List [nil] [cons Code List]]
 [Data Pair [p Tree List]]

 [Function fst Pair -> Tree]
 [Function snd Pair -> List]
 
 [Function append List List -> List]
 [Function enc Tree -> List]
 [Function dec List -> Tree]
 [Function decAux List -> Pair]
 }

{Lemma
 [A (Forall List xs (And (= (append nil xs) xs)
                         (= (append xs nil) xs)))]
 [D (Forall List xs (Forall List ys (Forall List zs
      (= (append xs (append ys zs))
         (append (append xs ys) zs)))))]
 [E (Forall Code x (Forall List xs (= (append (cons x nil) xs)
                                      (cons x xs))))]
 }

{Define
 [fst-d (fst (p x y)) x]
 [snd-d (snd (p x y)) y]
 
 [append-b (append nil l) l]
 [append-i (append (cons x xs) l) (cons x (append xs l))]

 [enc-l (enc (leaf i)) (cons (lf i) nil)]
 [enc-n (enc (node t1 t2)) (append (cons nd (enc t1))
                                   (enc t2))]

 [dec-d (dec cds)
        cases
        [(= nil (snd (decAux cds))) (fst (decAux cds))]
        [otherwise                  (leaf 0)]]

 [decAux-l (decAux (cons (lf i) cds)) (p (leaf i) cds)]
 [decAux-n (decAux (cons nd cds))
           (p (node (fst (decAux cds))
                    (fst (decAux (snd (decAux cds)))))
              (snd (decAux (snd (decAux cds)))))]
 }

{Proof (Forall Tree t (= (dec (enc t)) t))

  [G (Forall Tree t (= (dec (enc t)) t))
     (fst-d snd-d append-b append-i enc-l enc-n dec-d decAux-l decAux-n)]
  }

{Proof (Forall Tree t (= (dec (enc t)) t))

  [L Induction
     (Forall Tree t (Forall List cs
       (= (decAux (append (enc t) cs)) (p t cs))))
     
     [Take Int x
      Take List cs
      Show (= (decAux (append (enc (leaf x)) cs)) (p (leaf x) cs))
      By (append-b append-i enc-l decAux-l)]

     [Take Tree t1
      such that (Forall List cs (= (decAux (append (enc t1) cs)) (p t1 cs)))
      Take Tree t2
      such that (Forall List cs (= (decAux (append (enc t2) cs)) (p t2 cs)))
      Take List cs
      
      Show (= (decAux (append (enc (node t1 t2)) cs)) (p (node t1 t2) cs))

      [0 = (decAux (enc (node t1 t2)))
         = (decAux (append (cons nd (enc t1)) (enc t2))) (enc-n)
         = (decAux (cons nd (append (enc t1) (enc t2)))) (append-i)
         = (p (node (fst (decAux (append (enc t1) (enc t2))))
                    (fst (decAux (snd (decAux (append (enc t1) (enc t2)))))))
              (snd (decAux (snd (decAux (append (enc t1) (enc t2)))))))
           (decAux-n)]
      [1 = (decAux (append (enc (node t1 t2)) cs))
         = (decAux (append (append (cons nd (enc t1)) (enc t2)) cs)) (enc-n)
         = (decAux (cons nd (append (append (enc t1) (enc t2)) cs))) (append-i)
         
         = (p (node (fst (decAux
                          (append (append (enc t1) (enc t2)) cs)))
                    (fst (decAux
                          (snd (decAux
                                (append (append (enc t1) (enc t2)) cs))))))
              (snd (decAux
                    (snd (decAux
                          (append (append (enc t1) (enc t2)) cs))))))
           (decAux-n)
           
         = (p (node (fst (decAux
                          (append (enc t1) (append (enc t2) cs))))
                    (fst (decAux
                          (snd (decAux
                                (append (enc t1) (append (enc t2) cs)))))))
              (snd (decAux
                    (snd (decAux
                          (append (enc t1) (append (enc t2) cs)))))))
           (D)

         = (p (node (fst (p t1 (append (enc t2) cs)))
                    (fst (decAux (snd (p t1 (append (enc t2) cs))))))
              (snd (decAux (snd (p t1 (append (enc t2) cs))))))
           (%Hypo)

         = (p (node t1 (fst (decAux (append (enc t2) cs))))
              (snd (decAux (append (enc t2) cs))))
           (fst-d snd-d)

         = (p (node t1 (fst (p t2 cs)))
              (snd (p t2 cs)))
           (%Hypo)
           
         = (p (node t1 t2) cs) (fst-d snd-d)]
      ]
     ]

  [2 (Forall Tree t (= (decAux (append (enc t) nil)) (p t nil))) (L)]

  [G (Ind: (Forall Tree t (= (dec (enc t)) t)))
     (fst-d snd-d dec-d append-b append-i 2 A)]
  }