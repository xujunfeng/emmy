#lang reader "example-reader.rkt"

{Declare
 [Data List [nil] [cons Int List]]
 }

{Declare
 [Function append List List -> List]
 [Function rev List -> List]
 [Function revA List List -> List]
 }

{Define
 [append-1 (append nil l) l]
 [append-2 (append (cons i l1) l2) (cons i (append l1 l2))]

 [rev-1 (rev nil) nil]
 [rev-2 (rev (cons i l)) (append (rev l) (cons i nil))]

 [revA-1 (revA nil ys) ys]
 [revA-2 (revA (cons x xs) ys) (revA xs (cons x ys))]
 }

{Lemma
 [Append-Associative (Forall List xs
                       (Forall List ys
                         (Forall List zs
                           (= (append (append xs ys) zs)
                              (append xs (append ys zs))))))]
 }

{Proof (Forall List xs (= (revA xs nil) (rev xs)))
  [G (Forall List xs (= (revA xs nil) (rev xs)))
     (rev-1 rev-2 revA-1 revA-2 append-1 append-2 Append-Associative)]
  }

{Proof (Forall List xs (= (revA xs nil) (rev xs)))
  [L Induction
     (Forall List xs
       (Forall List ys (= (revA xs ys) (append (rev xs) ys))))
     [Take List ys
      Show (= (revA nil ys) (append (rev nil) ys))
      By (append-1 rev-1 revA-1)]
     [Take List xs
      such that (Forall List ys (= (revA xs ys) (append (rev xs) ys)))
      Take List ys
      Take Int x
      
      Show (= (revA (cons x xs) ys) (append (rev (cons x xs)) ys))
      By (append-1 append-2 rev-2 revA-2 Append-Associative %Hypo)]]

  [G (Forall List xs (= (revA xs nil) (rev xs)))
     (rev-1 rev-2 revA-1 revA-2 append-1 append-2 L)]
  }

{Proof (Forall List xs (= (revA xs nil) (rev xs)))
  [L (Ind: (Forall List xs
             (Forall List ys (= (revA xs ys) (append (rev xs) ys)))))
     (append-1 append-2 rev-1 rev-2 revA-1 revA-2 Append-Associative)]

  [G (Forall List xs (= (revA xs nil) (rev xs)))
     (rev-1 rev-2 revA-1 revA-2 append-1 append-2 L)]
  }