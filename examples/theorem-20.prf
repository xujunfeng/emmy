#lang reader "example-reader.rkt"

{Declare
 [Data Code [lf Int] [nd]]
 [Data Tree [node Tree Tree] [leaf Int]]
 [Data ListC [nilC] [consC Code ListC]]
 [Data ListT [nilT] [consT Tree ListT]]
 
 [Function appendC ListC ListC -> ListC]
 [Function appendT ListT ListT -> ListT]
 [Function encd Tree -> ListC]
 [Function decd ListC -> Tree]
 [Function decdAux ListC ListT -> Tree]
 }

{Lemma
 [DC (Forall ListC xs (Forall ListC ys (Forall ListC zs
       (= (appendC xs (appendC ys zs))
          (appendC (appendC xs ys) zs)))))]
 }

{Define
 [appendC-b (appendC nilC l) l]
 [appendC-i (appendC (consC x xs) l) (consC x (appendC xs l))]
 [appendT-b (appendT nilT l) l]
 [appendT-i (appendT (consT x xs) l) (consT x (appendT xs l))]

 [encd-l (encd (leaf i)) (consC (lf i) nilC)]
 [encd-n (encd (node t1 t2))
         (appendC (encd t1)
                  (appendC (encd t2)
                           (consC nd nilC)))]

 [decd-d (decd cds) (decdAux cds nilT)]

 [decdAux-b (decdAux nilC (consT t ts)) t]
 [decdAux-l (decdAux (consC (lf i) cds) ts)
            (decdAux cds (consT (leaf i) ts))]
 [decdAux-n (decdAux (consC nd cds) (consT t1 (consT t2 ts)))
            (decdAux cds (consT (node t2 t1) ts))]
 }

{Proof (Forall Tree t (= (decd (encd t)) t))
  [G (Forall Tree t (= (decd (encd t)) t))
     (appendC-b appendC-i appendT-b appendT-i
      encd-l encd-n decd-d decdAux-b decdAux-l decdAux-n)]}

{Proof (Forall Tree t (= (decd (encd t)) t))

  [L Induction
     (Forall Tree t (Forall ListC cds (Forall ListT ts
       (= (decdAux (appendC (encd t) cds) ts)
          (decdAux cds (consT t ts))))))

     [Take Int x
      Take ListC cds
      Take ListT ts

      Show (= (decdAux (appendC (encd (leaf x)) cds) ts)
              (decdAux cds (consT (leaf x) ts)))
      By (appendC-b appendC-i appendT-b encd-l decdAux-l)]

     [Take Tree t1 such that (Forall ListC cds (Forall ListT ts
                               (= (decdAux (appendC (encd t1) cds) ts)
                                  (decdAux cds (consT t1 ts)))))
      Take Tree t2 such that (Forall ListC cds (Forall ListT ts
                               (= (decdAux (appendC (encd t2) cds) ts)
                                  (decdAux cds (consT t2 ts)))))
      Take ListC cds
      Take ListT ts
      
      Show (= (decdAux (appendC (encd (node t1 t2)) cds) ts)
              (decdAux cds (consT (node t1 t2) ts)))

      [1 = (decdAux (appendC (encd (node t1 t2)) cds) ts)
         = (decdAux (appendC (appendC (encd t1)
                                      (appendC (encd t2)
                                               (consC nd nilC)))
                             cds)
                    ts)
           (encd-n)
           
         = (decdAux (appendC (encd t1)
                             (appendC (appendC (encd t2) (consC nd nilC))
                                      cds))
                    ts)
           (DC)

         = (decdAux (appendC (appendC (encd t2) (consC nd nilC))
                             cds)
                    (consT t1 ts))
           (%Hypo)

         = (decdAux (appendC (encd t2) (appendC (consC nd nilC) cds))
                    (consT t1 ts))
           (DC)

         = (decdAux (appendC (consC nd nilC) cds)
                    (consT t2 (consT t1 ts)))
           (%Hypo)

         = (decdAux (consC nd cds)
                    (consT t2 (consT t1 ts)))
           (appendC-b appendC-i)

         = (decdAux cds (consT (node t1 t2) ts)) (decdAux-n)]
      ]
     ]

  [G (Ind: (Forall Tree t (= (decd (encd t)) t)))
     (appendC-b appendC-i appendT-b appendT-i encd-l encd-n
      decd-d decdAux-b decdAux-l decdAux-n L)]
  }
